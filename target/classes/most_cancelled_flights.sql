SELECT json_extract(city, '$.ru') AS city, count(flight_id) AS cancelled_flights_count
FROM flights JOIN airports ON flights.departure_airport=airports.airport_code
WHERE status = 'Cancelled'
GROUP BY city
ORDER BY cancelled_flights_count DESC
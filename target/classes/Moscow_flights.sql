WITH Moscow_airports AS (
    SELECT airport_code
    FROM airports
    WHERE json_extract(city, '$.ru')='Москва'
)

SELECT CASE week_day
    WHEN '0' THEN 'Воскресенье'
    WHEN '1' THEN 'Понедельник'
    WHEN '2' THEN 'Вторник'
    WHEN '3' THEN 'Среда'
    WHEN '4' THEN 'Четверг'
    WHEN '5' THEN 'Пятница'
    WHEN '6' THEN 'Суббота'
END AS week_day, direction, count
FROM (
        SELECT strftime('%w', scheduled_departure||':00') AS week_day,
        'из Москвы' AS direction, COUNT(flight_id) AS count
        FROM flights
        WHERE departure_airport IN Moscow_airports
        GROUP BY week_day

        UNION

        SELECT strftime('%w', scheduled_arrival||':00') AS week_day,
        'в Москву' AS direction, COUNT (flight_id) AS count
        FROM flights
        WHERE arrival_airport IN Moscow_airports
        GROUP BY week_day
)
ORDER BY mod(cast(week_day AS integer) + 6, 7) ASC, direction ASC
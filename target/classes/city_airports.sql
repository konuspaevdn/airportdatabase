SELECT json_extract(city, '$.ru') AS city, group_concat(airport_code, ', ') AS airport_codes
FROM airports
GROUP BY city
HAVING count(airport_code) > 1
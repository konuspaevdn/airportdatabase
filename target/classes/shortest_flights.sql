WITH city_average_flight_duration AS (
    SELECT city, arrival_airport,
    avg(round((julianday(actual_arrival||':00') - julianday(actual_departure||':00')) * 24 * 60 * 60)) AS average_flight_duration
    FROM flights JOIN airports ON flights.departure_airport=airports.airport_code
    WHERE actual_arrival IS NOT NULL AND actual_departure IS NOT NULL
    GROUP BY city, arrival_airport
)

SELECT json_extract(city_average_flight_duration.city, '$.ru') AS city_departure,
       arrival_airport || '(' || json_extract(airports.city, '$.ru') || ')' AS destination_point,
       time(min(average_flight_duration), 'unixepoch') AS average_flight_duration
FROM city_average_flight_duration JOIN airports on city_average_flight_duration.arrival_airport=airports.airport_code
GROUP BY city_average_flight_duration.city
ORDER BY average_flight_duration ASC
WITH Moscow_airports AS (
    SELECT airport_code
    FROM airports
    WHERE json_extract(city, '$.ru')='Москва'
)

UPDATE flights
SET status = 'Cancelled'
WHERE (departure_airport IN Moscow_airports AND status NOT IN ('Departed', 'Arrived') AND
       strftime('%Y%m%d', scheduled_departure||':00') BETWEEN ? AND ?)
    OR (arrival_airport IN Moscow_airports AND status NOT IN ('Departed', 'Arrived') AND
        strftime('%Y%m%d', scheduled_arrival||':00') BETWEEN ? AND ?)
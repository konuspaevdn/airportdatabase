WITH cancelled_flights AS (
    SELECT flight_id
    FROM flights
    WHERE aircraft_code = ? AND status = 'Cancelled'
)
DELETE FROM boarding_passes
WHERE flight_id IN cancelled_flights;

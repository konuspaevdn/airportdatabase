DROP TABLE IF EXISTS aircrafts;
CREATE TABLE aircrafts
(
    aircraft_code char(3) NOT NULL,
    model         jsonb   NOT NULL,
    range         integer NOT NULL,
    PRIMARY KEY (aircraft_code)
);

DROP TABLE IF EXISTS airports;
CREATE TABLE airports
(
    airport_code char(3) NOT NULL,
    airport_name jsonb   NOT NULL,
    city         jsonb   NOT NULL,
    coordinates  point   NOT NULL,
    timezone     text    NOT NULL,
    PRIMARY KEY (airport_code)
);

DROP TABLE IF EXISTS bookings;
CREATE TABLE bookings
(
    book_ref     char(6)                  NOT NULL,
    book_date    timestamp with time zone NOT NULL,
    total_amount numeric(10, 2)           NOT NULL,
    PRIMARY KEY (book_ref)
);

DROP TABLE IF EXISTS tickets;
CREATE TABLE tickets
(
    ticket_no      char(13)    NOT NULL,
    book_ref       char(6)     NOT NULL,
    passenger_id   varchar(20) NOT NULL,
    passenger_name text        NOT NULL,
    contact_data   jsonb,
    PRIMARY KEY (ticket_no),
    FOREIGN KEY (book_ref) REFERENCES bookings
);

DROP TABLE IF EXISTS flights;
CREATE TABLE flights
(
    flight_id           serial                   NOT NULL,
    flight_no           char(6)                  NOT NULL,
    scheduled_departure timestamp with time zone NOT NULL,
    scheduled_arrival   timestamp with time zone NOT NULL,
    departure_airport   char(3)                  NOT NULL,
    arrival_airport     char(3)                  NOT NULL,
    status              varchar(20)              NOT NULL,
    aircraft_code       char(3)                  NOT NULL,
    actual_departure    timestamp with time zone,
    actual_arrival      timestamp with time zone,
    PRIMARY KEY (flight_id),
    FOREIGN KEY (aircraft_code) REFERENCES aircrafts,
    FOREIGN KEY (departure_airport) REFERENCES airports,
    FOREIGN KEY (arrival_airport) REFERENCES airports
);

DROP TABLE IF EXISTS ticket_flights;
CREATE TABLE ticket_flights
(
    ticket_no       char(13)       NOT NULL,
    flight_id       integer        NOT NULL,
    fare_conditions varchar(10)    NOT NULL,
    amount          numeric(10, 2) NOT NULL,
    PRIMARY KEY (ticket_no, flight_id),
    FOREIGN KEY (ticket_no) REFERENCES tickets,
    FOREIGN KEY (flight_id) REFERENCES flights
);

DROP TABLE IF EXISTS boarding_passes;
CREATE TABLE boarding_passes
(
    ticket_no   char(13)   NOT NULL,
    flight_id   integer    NOT NULL,
    boarding_no integer    NOT NULL,
    seat_no     varchar(4) NOT NULL,
    FOREIGN KEY (ticket_no, flight_id) REFERENCES ticket_flights
);

DROP TABLE IF EXISTS seats;
CREATE TABLE seats
(
    aircraft_code   char(3)     NOT NULL,
    seat_no         varchar(4)  NOT NULL,
    fare_conditions varchar(10) NOT NULL,
    FOREIGN KEY (aircraft_code) REFERENCES aircrafts
);
WITH cancelled_flights AS (
    SELECT flight_id
    FROM flights
    WHERE aircraft_code = ? AND status = 'Cancelled'
)
DELETE FROM ticket_flights
WHERE flight_id IN cancelled_flights;


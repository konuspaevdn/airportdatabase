SELECT CASE strftime('%m', scheduled_departure||':00')
    WHEN '01' THEN 'Январь'
    WHEN '02' THEN 'Февраль'
    WHEN '03' THEN 'Март'
    WHEN '04' THEN 'Апрель'
    WHEN '05' THEN 'Май'
    WHEN '06' THEN 'Июнь'
    WHEN '07' THEN 'Июль'
    WHEN '08' THEN 'Август'
    WHEN '09' THEN 'Сентябрь'
    WHEN '10' THEN 'Октябрь'
    WHEN '11' THEN 'Ноябрь'
    WHEN '12' THEN 'Декабрь'
END AS month,
count(flight_id) AS cancellation_count
FROM flights
WHERE status='Cancelled'
GROUP BY month
ORDER BY strftime('%m', scheduled_departure||':00')
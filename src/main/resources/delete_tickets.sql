WITH cancelled_flights AS (
    SELECT flight_id
    FROM flights
    WHERE aircraft_code = ? AND status = 'Cancelled'
),
     cancelled_ticket_flights AS (
         SELECT ticket_no
         FROM ticket_flights
         WHERE flight_id IN cancelled_flights
    )
DELETE FROM tickets
WHERE ticket_no IN cancelled_ticket_flights;
WITH flight_seats AS (
    SELECT boarding_no
    FROM boarding_passes
    WHERE flight_id = ?
)

SELECT ifnull((SELECT max(boarding_no) FROM flight_seats), 0) + 1 AS next_boarding_no
WITH Moscow_airports AS (
    SELECT airport_code
    FROM airports
    WHERE json_extract(city, '$.ru')='Москва'
)

SELECT strftime('%d.%m.%Y', scheduled_departure||':00') AS date, sum(amount) AS total_loss
FROM flights JOIN ticket_flights USING(flight_id)
WHERE status NOT IN ('Departed', 'Arrived', 'Cancelled') AND (
    (departure_airport IN Moscow_airports AND
     strftime('%Y%m%d', scheduled_departure||':00') BETWEEN ? AND ?)
    OR
    (arrival_airport IN Moscow_airports AND
     strftime('%Y%m%d', scheduled_arrival||':00') BETWEEN ? AND ?)
    )
GROUP BY strftime('%Y%m%d',scheduled_departure||':00')
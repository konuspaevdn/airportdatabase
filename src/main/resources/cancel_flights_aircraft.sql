UPDATE flights
SET status = 'Cancelled'
WHERE aircraft_code = ? AND status <> 'Arrived';
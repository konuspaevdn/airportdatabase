package konuspaev_dn.worker;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import konuspaev_dn.App;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.sql.DataSource;


public class DBFactory {
    private final static String pathToDB = "./sqlite/flights.db";
    private final static String tablesSrc = "/tables.sql";
    private static final HikariConfig config = new HikariConfig();
    private static final DataSource ds;


    static {
        config.setJdbcUrl("jdbc:sqlite:" + pathToDB);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        ds = new HikariDataSource(config);
        try {
            BuildDataBase();
            FillTables();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void BuildDataBase() throws SQLException, IOException {
        String createTables;
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(tablesSrc)),
                        StandardCharsets.UTF_8))) {
            createTables = br.lines().collect(Collectors.joining("\n"));
        }
        try (Connection connection = getConnection();
                Statement statement = connection.createStatement()) {
            statement.executeUpdate(createTables);
        }
    }
    private static void FillTables() throws SQLException, IOException {
        FillerAircrafts aircrafts = new FillerAircrafts();
        aircrafts.ReadCSV("sqlite/aircrafts_data.csv");
        aircrafts.FillTable();
        FillerAirports airports = new FillerAirports();
        airports.ReadCSV("sqlite/airports_data.csv");
        airports.FillTable();
        FillerBookings bookings = new FillerBookings();
        bookings.ReadCSV("sqlite/bookings.csv");
        bookings.FillTable();
        FillerTickets tickets = new FillerTickets();
        tickets.ReadCSV("sqlite/tickets.csv");
        tickets.FillTable();
        FillerFlights flights = new FillerFlights();
        flights.ReadCSV("sqlite/flights.csv");
        flights.FillTable();
        FillerTicketFlights ticket_flights = new FillerTicketFlights();
        ticket_flights.ReadCSV("sqlite/ticket_flights.csv");
        ticket_flights.FillTable();
        FillerBoardingPass boarding_passes = new FillerBoardingPass();
        boarding_passes.ReadCSV("sqlite/boarding_passes.csv");
        boarding_passes.FillTable();
        FillerSeats seats = new FillerSeats();
        seats.ReadCSV("sqlite/seats.csv");
        seats.FillTable();
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

}

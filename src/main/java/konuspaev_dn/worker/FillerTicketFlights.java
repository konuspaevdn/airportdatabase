package konuspaev_dn.worker;

import konuspaev_dn.dto.TicketFlight;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerTicketFlights implements Filler {
    ArrayList<TicketFlight> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"ticketNo", "flightID", "fareConditions", "amount"};
        CellProcessor[] procs = getProcessors();
        TicketFlight entry;
        while ((entry = csvBeanReader.read(TicketFlight.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(new ParseInt()),
                new NotNull(),
                new NotNull(new ParseDouble())
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO ticket_flights VALUES (?, ?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (TicketFlight ticketFlight : list) {
                statement.setString(1, ticketFlight.getTicketNo());
                statement.setInt(2, ticketFlight.getFlightID());
                statement.setString(3, ticketFlight.getFareConditions());
                statement.setDouble(4, ticketFlight.getAmount());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}

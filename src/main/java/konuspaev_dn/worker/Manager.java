package konuspaev_dn.worker;

import konuspaev_dn.dao.*;
import konuspaev_dn.dto.*;

import java.util.ArrayList;
import java.util.Set;

public class Manager {
    public static void QueryCitiesWithMultipleAirports() {
        try {
            CityAirportsDao impl = new CityAirportsDaoImpl();
            Set<CityAirports> set = impl.getCitiesWithMultipleAirports();
            ExcelLoader.buildCityAirportsTable(set);
        } catch (Exception e) {
            System.out.println("Error in QueryCitiesWithMultipleAirports: " + e.getMessage());
        }
    }

    public static void QueryMostCancelledFlights() {
        try {
            CancelledFlightsCountDao impl = new CancelledFlightsCountDaoImpl();
            ArrayList<CancelledFlightsCount> list = impl.getCitiesWithMostCancelledFlights();
            ExcelLoader.buildCancelledFlightsCountTable(list);
        } catch (Exception e) {
            System.out.println("Error in QueryMostCancelledFlights: " + e.getMessage());
        }
    }

    public static void QueryShortestFlights() {
        try {
            ShortestFlightDao impl = new ShortestFlightDaoImpl();
            ArrayList<ShortestFlight> list = impl.getShortestFlights();
            ExcelLoader.buildShortestFlightsTable(list);
        } catch (Exception e) {
            System.out.println("Error in QueryShortestFlights: " + e.getMessage());
        }
    }

    public static void QueryCancellationsByMonth() {
        try {
            MonthCancellationCountDao impl = new MonthCancellationCountDaoImpl();
            ArrayList<MonthCancellationCount> list = impl.getCancellationsByMonth();
            ExcelLoader.buildCancellationsByMonthTable(list);
            ChartBuilder.createCancellationsByMonthBarChart(list);
        } catch (Exception e) {
            System.out.println("Error in QueryCancellationsByMonth: " + e.getMessage());
        }
    }

    public static void QueryMoscowFlights() {
        try {
            MoscowFlightsDao impl = new MoscowFlightsDaoImpl();
            ArrayList<MoscowFlights> list = impl.getMoscowFlights();
            ExcelLoader.buildMoscowFlightsTable(list);
            ChartBuilder.createMoscowFlightsChart(list);
        } catch (Exception e) {
            System.out.println("Error in QueryMoscowFlights: " + e.getMessage());
        }
    }

    public static void QueryAircraftCancel(String aircraftCode) {
        try {
            CancelFlightsAircraftDao impl = new CancelFlightsAircraftDaoImpl();
            impl.CancelFlightsAndTickets(aircraftCode);
        } catch (Exception e) {
            System.out.println("Error in QueryAircraftCancel: " + e.getMessage());
        }
    }

    public static void QueryCancelMoscowFlightsAndCalculateLosses(String startDate, String endDate) {
        try {
            DayLossDao implCalculate = new DayLossDaoImpl();
            ArrayList<DayLoss> list = implCalculate.CalculateIntervalLossesAndCancelFlights(startDate, endDate);
            ExcelLoader.buildDayLossesTable(list);
            ChartBuilder.createDayLossesChart(list);
        } catch (Exception e) {
            System.out.println("Error in QueryCancelMoscowFlightsAndCalculateLosses: " + e.getMessage());
        }
    }

    public static void QueryInsertTicket(String[] info) {
        try {
            TicketInsertDao impl = new TicketInsertDaoImpl();
            impl.InsertTicket(info);
        } catch (Exception e) {
            System.out.println("Error in QueryInsertTicket: " + e.getMessage());
        }
    }
}

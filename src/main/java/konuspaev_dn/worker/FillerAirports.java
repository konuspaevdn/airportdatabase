package konuspaev_dn.worker;

import konuspaev_dn.dto.Airport;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerAirports implements Filler {
    ArrayList<Airport> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"airportCode", "airportName", "city", "coordinates", "timezone"};
        CellProcessor[] procs = getProcessors();
        Airport entry;
        while ((entry = csvBeanReader.read(Airport.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull()
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO airports VALUES (?, ?, ?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (Airport airport : list) {
                statement.setString(1, airport.getAirportCode());
                statement.setString(2, airport.getAirportName());
                statement.setString(3, airport.getCity());
                statement.setString(4, airport.getCoordinates());
                statement.setString(5, airport.getTimezone());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}

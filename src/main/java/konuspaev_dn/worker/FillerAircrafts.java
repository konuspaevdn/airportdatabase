package konuspaev_dn.worker;

import konuspaev_dn.dto.Aircraft;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerAircrafts implements Filler {
    ArrayList<Aircraft> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"aircraftCode", "model", "range"};
        CellProcessor[] procs = getProcessors();
        Aircraft entry;
        while ((entry = csvBeanReader.read(Aircraft.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new NotNull(new ParseInt())
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO aircrafts VALUES (?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (Aircraft aircraft : list) {
                statement.setString(1, aircraft.getAircraftCode());
                statement.setString(2, aircraft.getModel());
                statement.setInt(3, aircraft.getRange());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}

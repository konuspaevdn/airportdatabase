package konuspaev_dn.worker;

import konuspaev_dn.dto.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class ExcelLoader {
    private final static String OUTPUT_XLS_PATH = "./query_results/";

    private static void buildExcelTable(String heading, String[] columnNames,
                                 ArrayList<String[]> data, String filePath) throws IOException {
        try (Workbook book = new HSSFWorkbook()) {
            Sheet sheet = book.createSheet(heading);
            Row firstRow = sheet.createRow(0);

            CellStyle style = book.createCellStyle();
            Font font = book.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            style.setFont(font);
            style.setLocked(true);

            // Fill first row with columns names
            int j = 0;
            for (String rawCell : columnNames) {
                Cell currentCell = firstRow.createCell(j++);
                currentCell.setCellValue(rawCell);
                currentCell.setCellStyle(style);
            }

            // Fill all the rest rows
            int i = 1;
            for (String[] rawRow : data) {
                Row row = sheet.createRow(i++);
                j = 0;
                for (String rawCell : rawRow) {
                    Cell currentCell = row.createCell(j++);
                    currentCell.setCellValue(rawCell);
                }
            }

            // Resize columns to fit their data
            for (int x = 0; x < sheet.getRow(0).getPhysicalNumberOfCells(); x++) {
                sheet.autoSizeColumn(x);
            }

            // Save the table
            File file = new File(filePath);
            file.getParentFile().mkdirs();
            file.createNewFile();
            book.write(new FileOutputStream(filePath));
        }
    }

    public static void buildCityAirportsTable(Set<CityAirports> set) throws IOException {
        ArrayList<String[]> data = new ArrayList<>(set.size());
        for (CityAirports ca : set) {
            data.add(new String[]{ca.getCity(), ca.getAirportCodes()});
        }
        buildExcelTable("Cities with multiple airports", new String[]{"City", "List of airports"}, data,
                OUTPUT_XLS_PATH + "city_airports.xlsx");
    }

    public static void buildCancelledFlightsCountTable(ArrayList<CancelledFlightsCount> list) throws IOException {
        ArrayList<String[]> data = new ArrayList<>(list.size());
        for (CancelledFlightsCount cfc : list) {
            data.add(new String[]{cfc.getCity(), cfc.getCancelledFlightsCount().toString()});
        }
        buildExcelTable("Cities with most cancelled flights", new String[]{"City", "Count of cancelled flights"}, data,
                OUTPUT_XLS_PATH + "cancelled_flights_count.xlsx");
    }

    public static void buildShortestFlightsTable(ArrayList<ShortestFlight> list) throws IOException {
        ArrayList<String[]> data = new ArrayList<>(list.size());
        for (ShortestFlight sf : list) {
            data.add(new String[]{sf.getCityDeparture(), sf.getDestinationPoint(), sf.getAverageFlightDuration()});
        }
        buildExcelTable("Shortest flights", new String[]{"Departure city", "Destination point", "Average flight duration"}, data,
                OUTPUT_XLS_PATH + "shortest_flights.xlsx");
    }

    public static void buildCancellationsByMonthTable(ArrayList<MonthCancellationCount> list) throws IOException {
        ArrayList<String[]> data = new ArrayList<>(list.size());
        for (MonthCancellationCount mcc : list) {
            data.add(new String[]{mcc.getMonth(), mcc.getCount().toString()});
        }
        buildExcelTable("Cancellations by month", new String[]{"Month", "Cancellation count"}, data,
                OUTPUT_XLS_PATH + "cancellations_by_month.xlsx");
    }

    public static void buildMoscowFlightsTable(ArrayList<MoscowFlights> list) throws IOException {
        ArrayList<String[]> data = new ArrayList<>(list.size());
        for (MoscowFlights mf : list) {
            data.add(new String[]{mf.getWeekDay(), mf.getDirection(), mf.getCount().toString()});
        }
        buildExcelTable("Moscow flights", new String[]{"Week day", "Direction", "Count"}, data,
                OUTPUT_XLS_PATH + "Moscow_flights.xlsx");
    }

    public static void buildDayLossesTable(ArrayList<DayLoss> list) throws IOException {
        ArrayList<String[]> data = new ArrayList<>(list.size());
        for (DayLoss dl : list) {
            data.add(new String[]{dl.getDate(), dl.getLoss().toString()});
        }
        buildExcelTable("Covid-losses per day for Moscow flights", new String[]{"Date", "Total loss"}, data,
                OUTPUT_XLS_PATH + "Moscow_day_losses.xlsx");
    }
}




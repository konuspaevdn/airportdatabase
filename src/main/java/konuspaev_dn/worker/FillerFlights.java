package konuspaev_dn.worker;

import konuspaev_dn.dto.Flight;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerFlights implements Filler {
    ArrayList<Flight> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"flightID", "flightNo", "scheduledDeparture", "scheduledArrival", "departureAirport",
        "arrivalAirport", "status", "aircraftCode", "actualDeparture", "actualArrival"};
        CellProcessor[] procs = getProcessors();
        Flight entry;
        while ((entry = csvBeanReader.read(Flight.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(new ParseInt()),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new Optional(),
                new Optional()
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO flights VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (Flight flight : list) {
                statement.setInt(1, flight.getFlightID());
                statement.setString(2, flight.getFlightNo());
                statement.setString(3, flight.getScheduledDeparture());
                statement.setString(4, flight.getScheduledArrival());
                statement.setString(5, flight.getDepartureAirport());
                statement.setString(6, flight.getArrivalAirport());
                statement.setString(7, flight.getStatus());
                statement.setString(8, flight.getAircraftCode());
                statement.setString(9, flight.getActualDeparture());
                statement.setString(10, flight.getActualArrival());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}

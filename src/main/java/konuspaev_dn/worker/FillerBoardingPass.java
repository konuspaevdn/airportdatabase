package konuspaev_dn.worker;

import konuspaev_dn.dto.BoardingPass;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerBoardingPass implements Filler {
    ArrayList<BoardingPass> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"ticketNo", "flightID", "boardingNo", "seatNo"};
        CellProcessor[] procs = getProcessors();
        BoardingPass entry;
        while ((entry = csvBeanReader.read(BoardingPass.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(new ParseInt()),
                new NotNull(new ParseInt()),
                new NotNull()
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO boarding_passes VALUES (?, ?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (BoardingPass bp : list) {
                statement.setString(1, bp.getTicketNo());
                statement.setInt(2, bp.getFlightID());
                statement.setInt(3, bp.getBoardingNo());
                statement.setString(4, bp.getSeatNo());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}

package konuspaev_dn.worker;

import konuspaev_dn.dto.DayLoss;
import konuspaev_dn.dto.MonthCancellationCount;
import konuspaev_dn.dto.MoscowFlights;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ChartBuilder {
    private final static String OUTPUT_PNG_PATH = "./query_results/";
    private static final int chartWidth = 1920;
    private static final int chartHeight = 1080;

    private static DefaultCategoryDataset fillDatasetOneLabel(ArrayList<String[]> data) {
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (String[] row : data) {
            dataset.addValue(Double.parseDouble(row[1]), "default", row[0]);
        }
        return dataset;
    }

    private static DefaultCategoryDataset fillDatasetMultipleLabels(ArrayList<String[]> data) {
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (String[] row : data) {
            dataset.addValue(Double.parseDouble(row[2]), row[1], row[0]);
        }
        return dataset;
    }

    private static void configureChart(JFreeChart barChart, String title, int labelSize, boolean set_legend) {
        CategoryPlot plot = barChart.getCategoryPlot();
        CategoryAxis axis = plot.getDomainAxis();

        Font font = new Font("Cambria", Font.BOLD, labelSize);
        axis.setTickLabelFont(font);
        Font font3 = new Font("Cambria", Font.BOLD, 30);
        barChart.setTitle(new org.jfree.chart.title.TextTitle(title, new java.awt.Font("Cambria", java.awt.Font.BOLD, 40)));

        plot.getDomainAxis().setLabelFont(font3);
        plot.getRangeAxis().setLabelFont(font3);
        CategoryPlot categoryPlot = (CategoryPlot) barChart.getPlot();
        BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();
        renderer.setBarPainter(new StandardBarPainter());
        if (!set_legend) return;
        LegendTitle legend = new LegendTitle(plot.getRenderer());
        Font font4 = new Font("Arial", Font.PLAIN,20);
        legend.setItemFont(font4);
        barChart.removeLegend();
        barChart.addLegend(legend);
    }

    public static void createCancellationsByMonthBarChart(ArrayList<MonthCancellationCount> list) throws IOException {
        ArrayList<String[]> rawData = new ArrayList<>(list.size());
        for (MonthCancellationCount mcc : list) {
            rawData.add(new String[]{mcc.getMonth(), mcc.getCount().toString()});
        }
        DefaultCategoryDataset dataset = fillDatasetOneLabel(rawData);
        String title = "Cancellations by month";
        String categoryAxis = "Month";
        String valueAxis = "Count";

        JFreeChart barChart = ChartFactory.createBarChart(title, categoryAxis, valueAxis, dataset,
                PlotOrientation.VERTICAL, false, false, false);
        int labelSize = 30;
        configureChart(barChart, title, labelSize, false);
        File file = new File(OUTPUT_PNG_PATH + "cancellations_by_month_chart.png");
        file.getParentFile().mkdirs();
        file.createNewFile();
        ChartUtils.saveChartAsPNG(file, barChart, chartWidth, chartHeight);
    }

    public static void createMoscowFlightsChart(ArrayList<MoscowFlights> list) throws IOException {
        ArrayList<String[]> rawData = new ArrayList<>(list.size());
        for (MoscowFlights mf : list) {
            rawData.add(new String[]{mf.getWeekDay(), mf.getDirection(), mf.getCount().toString()});
        }
        DefaultCategoryDataset dataset = fillDatasetMultipleLabels(rawData);
        String title = "Moscow flights";
        String categoryAxis = "Week day";
        String valueAxis = "Count";

        JFreeChart barChart = ChartFactory.createBarChart(title, categoryAxis, valueAxis, dataset,
                PlotOrientation.VERTICAL, true, false, false);
        int labelSize = 20;
        configureChart(barChart, title, labelSize, true);
        File file = new File(OUTPUT_PNG_PATH + "Moscow_flights_chart.png");
        file.getParentFile().mkdirs();
        file.createNewFile();
        ChartUtils.saveChartAsPNG(file, barChart, chartWidth, chartHeight);
    }

    public static void createDayLossesChart(ArrayList<DayLoss> list) throws IOException {
        ArrayList<String[]> rawData = new ArrayList<>(list.size());
        for (DayLoss dl : list) {
            rawData.add(new String[]{dl.getDate(), dl.getLoss().toString()});
        }
        DefaultCategoryDataset dataset = fillDatasetOneLabel(rawData);
        String title = "Covid-losses per day for Moscow flights";
        String categoryAxis = "Date";
        String valueAxis = "Total loss, RUB";

        JFreeChart barChart = ChartFactory.createBarChart(title, categoryAxis, valueAxis, dataset,
                PlotOrientation.VERTICAL, false, false, false);
        int labelSize = 25;
        configureChart(barChart, title, labelSize, false);
        File file = new File(OUTPUT_PNG_PATH + "Moscow_day_losses_chart.png");
        file.getParentFile().mkdirs();
        file.createNewFile();
        ChartUtils.saveChartAsPNG(file, barChart, chartWidth, chartHeight);
    }
}
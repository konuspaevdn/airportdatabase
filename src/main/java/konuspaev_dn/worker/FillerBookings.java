package konuspaev_dn.worker;

import konuspaev_dn.dto.Booking;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerBookings implements Filler {
    ArrayList<Booking> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"bookRef", "bookDate", "totalAmount"};
        CellProcessor[] procs = getProcessors();
        Booking entry;
        while ((entry = csvBeanReader.read(Booking.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new NotNull(new ParseDouble())
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO bookings VALUES (?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (Booking booking : list) {
                statement.setString(1, booking.getBookRef());
                statement.setString(2, booking.getBookDate());
                statement.setDouble(3, booking.getTotalAmount());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}


package konuspaev_dn.worker;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public interface Filler {
    void ReadCSV(String fileName) throws IOException;

    void FillTable() throws SQLException;
}


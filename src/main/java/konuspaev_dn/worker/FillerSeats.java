package konuspaev_dn.worker;

import konuspaev_dn.dto.Seat;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerSeats implements Filler {
    ArrayList<Seat> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"aircraftCode", "seatNo", "fareConditions"};
        CellProcessor[] procs = getProcessors();
        Seat entry;
        while ((entry = csvBeanReader.read(Seat.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new NotNull()
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO seats VALUES (?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (Seat seat : list) {
                statement.setString(1, seat.getAircraftCode());
                statement.setString(2, seat.getSeatNo());
                statement.setString(3, seat.getFareConditions());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}

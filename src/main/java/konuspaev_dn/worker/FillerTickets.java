package konuspaev_dn.worker;

import konuspaev_dn.dto.Ticket;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FillerTickets implements Filler {
    ArrayList<Ticket> list = new ArrayList<>();
    @Override
    public void ReadCSV(String fileName) throws IOException {
        ICsvBeanReader csvBeanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);

        String[] mapping = new String[]{"ticketNo", "bookRef", "passengerID", "passengerName", "contactData"};
        CellProcessor[] procs = getProcessors();
        Ticket entry;
        while ((entry = csvBeanReader.read(Ticket.class, mapping, procs)) != null) {
            list.add(entry);
        }
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new Optional()
        };
    }

    @Override
    public void FillTable() throws SQLException {
        try (Connection connection = DBFactory.getConnection();
                PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO tickets VALUES (?, ?, ?, ?, ?)")) {
            connection.setAutoCommit(false);
            for (Ticket ticket : list) {
                statement.setString(1, ticket.getTicketNo());
                statement.setString(2, ticket.getBookRef());
                statement.setString(3, ticket.getPassengerID());
                statement.setString(4, ticket.getPassengerName());
                statement.setString(5, ticket.getContactData());
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }
}


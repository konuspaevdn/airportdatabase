package konuspaev_dn.dto;

public class CancelledFlightsCount {
    private String city;
    private Integer cancelledFlightsCount;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCancelledFlightsCount() {
        return cancelledFlightsCount;
    }

    public void setCancelledFlightsCount(int cancelledFlightsCount) {
        this.cancelledFlightsCount = cancelledFlightsCount;
    }

    @Override
    public String toString() {
        return "City=" + getCity() + ";count=" + getCancelledFlightsCount();
    }
}

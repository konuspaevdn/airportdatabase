package konuspaev_dn.dto;

public class CityAirports {
    private String city;
    private String airportCodes;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAirportCodes() {
        return airportCodes;
    }

    public void setAirportCodes(String airportCodes) {
        this.airportCodes = airportCodes;
    }

    @Override
    public String toString() {
        return "City=" + getCity() + ";AirportCodes=\"" + getAirportCodes() + "\"";
    }
}

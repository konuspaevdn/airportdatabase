package konuspaev_dn.dto;

public class ShortestFlight {
    private String cityDeparture;
    private String destinationPoint;
    private String averageFlightDuration;

    public String getCityDeparture() {
        return cityDeparture;
    }

    public void setCityDeparture(String cityDeparture) {
        this.cityDeparture = cityDeparture;
    }

    public String getDestinationPoint() {
        return destinationPoint;
    }

    public void setDestinationPoint(String destinationPoint) {
        this.destinationPoint = destinationPoint;
    }

    public String getAverageFlightDuration() {
        return averageFlightDuration;
    }

    public void setAverageFlightDuration(String averageFlightDuration) {
        this.averageFlightDuration = averageFlightDuration;
    }
}

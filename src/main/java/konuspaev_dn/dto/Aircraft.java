package konuspaev_dn.dto;

public class Aircraft {
    private String aircraftCode;
    private String model;
    private int range;

    public String getAircraftCode() {
        return aircraftCode;
    }

    public void setAircraftCode(String aircraft_code) {
        this.aircraftCode = aircraft_code;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String toString() {
        return "Code=" + getAircraftCode() + ";Model=" + getModel() + ";Range=" + getRange();
    }
}

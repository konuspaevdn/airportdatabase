package konuspaev_dn.dao;

import konuspaev_dn.dto.MonthCancellationCount;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface MonthCancellationCountDao {
    ArrayList<MonthCancellationCount> getCancellationsByMonth() throws SQLException, IOException;
}

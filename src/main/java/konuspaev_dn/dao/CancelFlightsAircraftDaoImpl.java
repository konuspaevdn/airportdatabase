package konuspaev_dn.dao;

import konuspaev_dn.App;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class CancelFlightsAircraftDaoImpl implements CancelFlightsAircraftDao {
    @Override
    public void CancelFlightsAndTickets(String aircraftCode) throws SQLException, IOException {
        String queryUpdate;
        String queryUpdateSrc = "/cancel_flights_aircraft.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(queryUpdateSrc)),
                        StandardCharsets.UTF_8))) {
            queryUpdate = br.lines().collect(Collectors.joining("\n"));
        }
        String queryDeleteTickets;
        String queryDeleteTicketsSrc = "/delete_tickets.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(queryDeleteTicketsSrc)),
                        StandardCharsets.UTF_8))) {
            queryDeleteTickets = br.lines().collect(Collectors.joining("\n"));
        }
        String queryDeleteTicketFlights;
        String queryDeleteTicketFlightsSrc = "/delete_ticket_flights.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(queryDeleteTicketFlightsSrc)),
                        StandardCharsets.UTF_8))) {
            queryDeleteTicketFlights = br.lines().collect(Collectors.joining("\n"));
        }
        String queryDeleteBoardingPasses;
        String queryDeleteBoardingPassesSrc = "/delete_boarding_passes.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(queryDeleteBoardingPassesSrc)),
                        StandardCharsets.UTF_8))) {
            queryDeleteBoardingPasses = br.lines().collect(Collectors.joining("\n"));
        }
        try (Connection connection = getConnection();
             PreparedStatement statUpdate = connection.prepareStatement(queryUpdate);
             PreparedStatement statDeleteTickets = connection.prepareStatement(queryDeleteTickets);
             PreparedStatement statDeleteTicketFlights = connection.prepareStatement(queryDeleteTicketFlights);
             PreparedStatement statDeleteBoardingPasses = connection.prepareStatement(queryDeleteBoardingPasses)) {
            connection.setAutoCommit(false);
            statUpdate.setString(1, aircraftCode);
            statUpdate.executeUpdate();
            statDeleteTickets.executeUpdate();
            statDeleteTicketFlights.executeUpdate();
            statDeleteBoardingPasses.executeUpdate();
            connection.commit();
        }
    }
}

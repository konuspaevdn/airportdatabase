package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.CityAirports;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class CityAirportsDaoImpl implements CityAirportsDao {
    @Override
    public Set<CityAirports> getCitiesWithMultipleAirports() throws SQLException, IOException {
        String query;
        String querySrc = "/city_airports.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(querySrc)),
                        StandardCharsets.UTF_8))) {
            query = br.lines().collect(Collectors.joining("\n"));
        }
        Set<CityAirports> set = new HashSet<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    set.add(extractCityAirports(rs));
                }
        }
        return set;
    }

    CityAirports extractCityAirports(ResultSet rs) throws SQLException {
        CityAirports ca = new CityAirports();
        ca.setCity(rs.getString("city"));
        ca.setAirportCodes(rs.getString("airport_codes"));
        return ca;
    }
}

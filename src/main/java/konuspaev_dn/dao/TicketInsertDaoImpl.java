package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.*;

import konuspaev_dn.custom_exceptions.DataException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class TicketInsertDaoImpl implements TicketInsertDao {

    /**
     *
     * @param info - all the information about an inserted ticket related to passenger:
     *             info[0] - ticket_no
     *             info[1] - flight_id
     *             info[2] - seat_no
     *             info[3] - passenger_id
     *             info[4] - passenger_name
     *             info[5] - contact_data
     *             info[6] - book_ref
     *             info[7] - amount
     *
     * @throws DataException when input flight or seat does not exist
     */
    @Override
    public void InsertTicket(String[] info) throws SQLException, IOException, DataException {
        try (Connection connection = getConnection()) {
            connection.setAutoCommit(false);

            Flight flight = new Flight();
            flight.setFlightID(Integer.parseInt(info[1]));
            if (!FlightExists(flight, connection)) {
                throw new DataException(flight.getFlightID() + ": flight with such ID does not exist");
            }

            Seat seat = new Seat();
            seat.setSeatNo(info[2]);
            seat.setAircraftCode(flight.getAircraftCode());
            if (!SeatExists(seat, connection)) {
                throw new DataException(seat.getSeatNo() + ": seat with such № does not exist on aircraft "
                        + seat.getAircraftCode());
            }

            Booking booking = new Booking();
            booking.setBookRef(info[6]);
            booking.setTotalAmount(Double.parseDouble(info[7]));
            ProcessBooking(booking, connection);

            Ticket ticket = new Ticket();
            ticket.setTicketNo(info[0]);
            ticket.setBookRef(info[6]);
            ticket.setPassengerID(info[3]);
            ticket.setPassengerName(info[4]);
            ticket.setContactData(info[5]);
            ProcessTicket(ticket, connection);

            TicketFlight ticketFlight = new TicketFlight();
            ticketFlight.setTicketNo(info[0]);
            ticketFlight.setFlightID(Integer.parseInt(info[1]));
            ticketFlight.setFareConditions(seat.getFareConditions());
            ticketFlight.setAmount(Double.parseDouble(info[7]));
            ProcessTicketFlight(ticketFlight, connection);

            BoardingPass boardingPass = new BoardingPass();
            boardingPass.setTicketNo(info[0]);
            boardingPass.setFlightID(Integer.parseInt(info[1]));
            boardingPass.setBoardingNo(GetNextBoardingNo(boardingPass.getFlightID(), connection));
            boardingPass.setSeatNo(info[2]);
            ProcessBoardingPass(boardingPass, connection);

            connection.commit();
        }
    }

    @Override
    public boolean FlightExists(Flight flight, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("SELECT aircraft_code " +
                                                                       "FROM flights WHERE flight_id = ?")) {
            statement.setInt(1, flight.getFlightID());
            ResultSet rs = statement.executeQuery();
            if (!rs.next()) {
                return false;
            }
            flight.setAircraftCode(rs.getString("aircraft_code"));
        }
        return true;
    }

    @Override
    public boolean SeatExists(Seat seat, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("SELECT fare_conditions " +
                                                                       "FROM seats WHERE seat_no = ? " +
                                                                       "AND aircraft_code = ?")) {
            statement.setString(1, seat.getSeatNo());
            statement.setString(2, seat.getAircraftCode());
            ResultSet rs = statement.executeQuery();
            if (!rs.next()) {
                return false;
            }
            seat.setFareConditions(rs.getString("fare_conditions"));
        }
        return true;
    }

    @Override
    public void ProcessBooking(Booking booking, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("SELECT total_amount FROM bookings " +
                                                                       "WHERE book_ref = ? ")) {
            statement.setString(1, booking.getBookRef());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                double totalAmount = rs.getDouble("total_amount");
                totalAmount += booking.getTotalAmount();
                try (PreparedStatement statUpdate = connection.prepareStatement("UPDATE bookings " +
                                                                                "SET total_amount = ? " +
                                                                                "WHERE book_ref = ?")) {
                    statUpdate.setDouble(1, totalAmount);
                    statUpdate.setString(2, booking.getBookRef());
                    statUpdate.executeUpdate();
                }
            } else {
                try (PreparedStatement statInsert = connection.prepareStatement("INSERT INTO bookings " +
                                                                                "VALUES (?, CURRENT_TIMESTAMP||'+03', ?)")) {
                    statInsert.setString(1, booking.getBookRef());
                    statInsert.setDouble(2, booking.getTotalAmount());
                    statInsert.executeUpdate();
                }
            }
        }
    }

    @Override
    public void ProcessTicket(Ticket ticket, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("SELECT ticket_no FROM tickets " +
                                                                        "WHERE ticket_no = ?")) {
            statement.setString(1, ticket.getTicketNo());
            ResultSet rs = statement.executeQuery();
            if (!rs.next()) {
                try (PreparedStatement statInsert = connection.prepareStatement("INSERT INTO tickets " +
                                                                                "VALUES (?, ?, ?, ?, ?)")) {
                    statInsert.setString(1, ticket.getTicketNo());
                    statInsert.setString(2, ticket.getBookRef());
                    statInsert.setString(3, ticket.getPassengerID());
                    statInsert.setString(4, ticket.getPassengerName());
                    statInsert.setString(5, ticket.getContactData());
                    statInsert.executeUpdate();
                }
            }
        }
    }

    @Override
    public void ProcessTicketFlight(TicketFlight ticketFlight, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO ticket_flights " +
                                                                       "VALUES (?, ?, ?, ?)")) {
            statement.setString(1, ticketFlight.getTicketNo());
            statement.setInt(2, ticketFlight.getFlightID());
            statement.setString(3, ticketFlight.getFareConditions());
            statement.setDouble(4, ticketFlight.getAmount());
            statement.executeUpdate();
        }
    }

    @Override
    public int GetNextBoardingNo(int flightID, Connection connection) throws SQLException, IOException {
        String query;
        String querySrc = "/next_boarding_no.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(querySrc)),
                        StandardCharsets.UTF_8))) {
            query = br.lines().collect(Collectors.joining("\n"));
        }
        int result;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, flightID);
            ResultSet rs = statement.executeQuery();
            rs.next();
            result = rs.getInt("next_boarding_no");
        }
        return result;
    }

    @Override
    public void ProcessBoardingPass(BoardingPass boardingPass, Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO boarding_passes " +
                                                                       "VALUES (?, ?, ?, ?)")) {
            statement.setString(1, boardingPass.getTicketNo());
            statement.setInt(2, boardingPass.getFlightID());
            statement.setInt(3, boardingPass.getBoardingNo());
            statement.setString(4, boardingPass.getSeatNo());
            statement.executeUpdate();
        }
    }
}

package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.MoscowFlights;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class MoscowFlightsDaoImpl implements MoscowFlightsDao {

    @Override
    public ArrayList<MoscowFlights> getMoscowFlights() throws SQLException, IOException {
        String query;
        String querySrc = "/Moscow_flights.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(querySrc)),
                        StandardCharsets.UTF_8))) {
            query = br.lines().collect(Collectors.joining("\n"));
        }
        ArrayList<MoscowFlights> list = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                list.add(extractMoscowFlights(rs));
            }
        }
        return list;
    }

    MoscowFlights extractMoscowFlights(ResultSet rs) throws SQLException {
        MoscowFlights mf = new MoscowFlights();
        mf.setWeekDay(rs.getString("week_day"));
        mf.setDirection(rs.getString("direction"));
        mf.setCount(rs.getInt("count"));
        return mf;
    }
}


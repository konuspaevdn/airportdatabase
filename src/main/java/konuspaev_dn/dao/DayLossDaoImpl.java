package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.DayLoss;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class DayLossDaoImpl implements DayLossDao {
    @Override
    public ArrayList<DayLoss> CalculateIntervalLossesAndCancelFlights(String startDate, String endDate)
            throws SQLException, IOException {
        String queryCalculate;
        String queryCalculateSrc = "/losses_interval_Moscow.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(queryCalculateSrc)),
                        StandardCharsets.UTF_8))) {
            queryCalculate = br.lines().collect(Collectors.joining("\n"));
        }
        String queryCancel;
        String queryCancelSrc = "/cancel_flights_interval_Moscow.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(queryCancelSrc)),
                        StandardCharsets.UTF_8))) {
            queryCancel = br.lines().collect(Collectors.joining("\n"));
        }

        ArrayList<DayLoss> list = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement statCalculate = connection.prepareStatement(queryCalculate);
             PreparedStatement statCancel = connection.prepareStatement(queryCancel)) {
            connection.setAutoCommit(false);
            statCalculate.setString(1, formatDate(startDate));
            statCalculate.setString(2, formatDate(endDate));
            statCalculate.setString(3, formatDate(startDate));
            statCalculate.setString(4, formatDate(endDate));
            ResultSet rs = statCalculate.executeQuery();
            while (rs.next()) {
                list.add(extractDayLoss(rs));
            }

            statCancel.setString(1, formatDate(startDate));
            statCancel.setString(2, formatDate(endDate));
            statCancel.setString(3, formatDate(startDate));
            statCancel.setString(4, formatDate(endDate));
            statCancel.executeUpdate();
            connection.commit();
        }
        return list;
    }

    String formatDate(String date) {  // expected date format is "DD.MM.YYYY"
        String year = date.substring(6, 10);
        String month = date.substring(3, 5);
        String day = date.substring(0, 2);
        return year + month + day;
    }

    DayLoss extractDayLoss(ResultSet rs) throws SQLException {
        DayLoss dl = new DayLoss();
        dl.setDate(rs.getString("date"));
        dl.setLoss(rs.getDouble("total_loss"));
        return dl;
    }
}

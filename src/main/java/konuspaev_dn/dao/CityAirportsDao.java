package konuspaev_dn.dao;

import konuspaev_dn.dto.CityAirports;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

public interface CityAirportsDao {
    Set<CityAirports> getCitiesWithMultipleAirports() throws SQLException, IOException;
}

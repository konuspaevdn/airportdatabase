package konuspaev_dn.dao;

import konuspaev_dn.dto.CancelledFlightsCount;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface CancelledFlightsCountDao {
    ArrayList<CancelledFlightsCount> getCitiesWithMostCancelledFlights() throws SQLException, IOException;
}

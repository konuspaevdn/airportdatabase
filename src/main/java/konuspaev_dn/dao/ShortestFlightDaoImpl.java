package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.ShortestFlight;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class ShortestFlightDaoImpl implements ShortestFlightDao {

    @Override
    public ArrayList<ShortestFlight> getShortestFlights() throws SQLException, IOException {
        String query;
        String querySrc = "/shortest_flights.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(querySrc)),
                        StandardCharsets.UTF_8))) {
                query = br.lines().collect(Collectors.joining("\n"));
        }
        ArrayList<ShortestFlight> list = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    list.add(extractShortestFlight(rs));
                }
        }
        return list;
    }

    ShortestFlight extractShortestFlight(ResultSet rs) throws SQLException {
        ShortestFlight sf = new ShortestFlight();
        sf.setCityDeparture(rs.getString("city_departure"));
        sf.setDestinationPoint(rs.getString("destination_point"));
        sf.setAverageFlightDuration(rs.getString("average_flight_duration"));
        return sf;
    }
}

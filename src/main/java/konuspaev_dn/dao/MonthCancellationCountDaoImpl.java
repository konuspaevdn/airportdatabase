package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.MonthCancellationCount;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class MonthCancellationCountDaoImpl implements MonthCancellationCountDao {
    @Override
    public ArrayList<MonthCancellationCount> getCancellationsByMonth() throws SQLException, IOException {
        String query;
        String querySrc = "/cancellations_by_month.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(querySrc)),
                        StandardCharsets.UTF_8))) {
            query = br.lines().collect(Collectors.joining("\n"));
        }
        ArrayList<MonthCancellationCount> list = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                list.add(extractMonthCancellationCount(rs));
            }
        }
        return list;
    }

    MonthCancellationCount extractMonthCancellationCount(ResultSet rs) throws SQLException {
        MonthCancellationCount mcc = new MonthCancellationCount();
        mcc.setMonth(rs.getString("month"));
        mcc.setCount(rs.getInt("cancellation_count"));
        return mcc;
    }
}

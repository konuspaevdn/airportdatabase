package konuspaev_dn.dao;

import konuspaev_dn.dto.ShortestFlight;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ShortestFlightDao {
    ArrayList<ShortestFlight> getShortestFlights() throws SQLException, IOException;
}

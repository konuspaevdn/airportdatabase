package konuspaev_dn.dao;

import konuspaev_dn.dto.*;

import konuspaev_dn.custom_exceptions.DataException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public interface TicketInsertDao {

    /**
     *
     * @param info - all the information about an inserted ticket related to passenger:
     *             info[0] - ticket_no
     *             info[1] - flight_id
     *             info[2] - seat_no
     *             info[3] - passenger_id
     *             info[4] - passenger_name
     *             info[5] - contact_data
     *             info[6] - book_ref
     *             info[7] - amount
     *
     * @throws DataException when input flight or seat does not exist
     */
    void InsertTicket(final String[] info) throws SQLException, IOException, DataException;

    boolean FlightExists(Flight flight, Connection connection) throws SQLException;

    boolean SeatExists(Seat seat, Connection connection) throws SQLException;

    void ProcessBooking(Booking booking, Connection connection) throws SQLException;

    void ProcessTicket(Ticket ticket, Connection connection) throws SQLException;

    void ProcessTicketFlight(TicketFlight ticketFlight, Connection connection) throws SQLException;

    int GetNextBoardingNo(int flightID, Connection connection) throws SQLException, IOException;

    void ProcessBoardingPass(BoardingPass boardingPass, Connection connection) throws SQLException;
}

package konuspaev_dn.dao;

import konuspaev_dn.dto.MoscowFlights;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface MoscowFlightsDao {
    ArrayList<MoscowFlights> getMoscowFlights() throws SQLException, IOException;
}

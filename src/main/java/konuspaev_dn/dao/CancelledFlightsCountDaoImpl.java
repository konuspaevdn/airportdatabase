package konuspaev_dn.dao;

import konuspaev_dn.App;
import konuspaev_dn.dto.CancelledFlightsCount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;

import static konuspaev_dn.worker.DBFactory.getConnection;

public class CancelledFlightsCountDaoImpl implements CancelledFlightsCountDao {
    @Override
    public ArrayList<CancelledFlightsCount> getCitiesWithMostCancelledFlights() throws SQLException, IOException {
        String query;
        String querySrc = "/most_cancelled_flights.sql";
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(App.class.getResourceAsStream(querySrc)),
                        StandardCharsets.UTF_8))) {
            query = br.lines().collect(Collectors.joining("\n"));
        }
        ArrayList<CancelledFlightsCount> list = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                list.add(extractCancelledFlightsCount(rs));
            }
        }
        return list;
    }

    CancelledFlightsCount extractCancelledFlightsCount(ResultSet rs) throws SQLException {
        CancelledFlightsCount cfc = new CancelledFlightsCount();
        cfc.setCity(rs.getString("city"));
        cfc.setCancelledFlightsCount(rs.getInt("cancelled_flights_count"));
        return cfc;
    }
}

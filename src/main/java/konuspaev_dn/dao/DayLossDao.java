package konuspaev_dn.dao;

import konuspaev_dn.dto.DayLoss;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public interface DayLossDao {
    ArrayList<DayLoss> CalculateIntervalLossesAndCancelFlights(String startDate, String endDate)
            throws SQLException, IOException;
}

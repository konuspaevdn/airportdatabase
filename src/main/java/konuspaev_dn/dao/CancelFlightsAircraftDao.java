package konuspaev_dn.dao;

import java.io.IOException;
import java.sql.SQLException;

public interface CancelFlightsAircraftDao {
    void CancelFlightsAndTickets(String aircraftCode) throws SQLException, IOException;
}

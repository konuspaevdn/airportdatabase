package konuspaev_dn.custom_exceptions;

public class DataException extends Exception {
    public DataException(String errorMessage) {
        super(errorMessage);
    }
}
